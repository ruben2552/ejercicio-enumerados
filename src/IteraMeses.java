
public class IteraMeses {

	public static void main(String[] args) {
		for (MesesDelAnio mes : MesesDelAnio.values()) {

			if (!mes.isPrimavera()) {
				System.out.println(mes + " -> " + mes.getEstacion());
			}

		}
	}

}
