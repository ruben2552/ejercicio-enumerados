
public enum MesesDelAnio {
	ENERO(EstacionesDelAnio.INVIERNO, 31), 
	FEBRERO(EstacionesDelAnio.INVIERNO, 28),
	MARZO(EstacionesDelAnio.PRIMAVERA, 31), 
	ABRIL(EstacionesDelAnio.PRIMAVERA, 30),
	MAYO(EstacionesDelAnio.PRIMAVERA, 31),
	JUNIO(EstacionesDelAnio.VERANO, 30),
	JULIO(EstacionesDelAnio.VERANO, 31), 
	AGOSTO(EstacionesDelAnio.VERANO, 31),
	SEPTIEMBRE(EstacionesDelAnio.OTONYO, 30),
	OCTUBRE(EstacionesDelAnio.OTONYO, 31), 
	NOVIEMBRE(EstacionesDelAnio.OTONYO, 30),
	DICIEMBRE(EstacionesDelAnio.INVIERNO, 31);
	
	private final EstacionesDelAnio estacionAņo;
	private final int numDiasMes;
		
	private MesesDelAnio(EstacionesDelAnio _estacion, int _numDiasMes) {
		this.estacionAņo = _estacion;
		this.numDiasMes = _numDiasMes;
	}
	
	public EstacionesDelAnio getEstacion() {
		return this.estacionAņo;
	}
	
	public boolean isPrimavera() {
		boolean esPrimavera = false;
		
		if (this.estacionAņo == this.estacionAņo.PRIMAVERA) {
			esPrimavera = true;
		}
		return esPrimavera;
	}
}
